var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}

"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {


    onPageLoading();


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function (paramRes) {
            console.log(paramRes);
            onLoadData(paramRes);

        },
        error: function (paramError) {
            console.log(paramError.status);
        }
    })
}
function onLoadData(paramRes) {

    var vCountTrend = 0;
    var vCountPop = 0;
    var vIndex = 0;
    while (vCountPop < 4) {
        if (paramRes[vIndex].isTrending == true) {

            getDataTrending(paramRes, vIndex);
            vCountTrend++;
        }
        if (paramRes[vIndex].isPopular == true) {
            getDataPopular(paramRes, vIndex);
            vCountPop++;
        }
        vIndex++;
    }
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getDataTrending(paramRes, paramIndex) {

    $("#row-trending").append($("<div>", {
        class: "col-sm-3",

    })
        .append($("<div>", {
            class: "card card-demo",

        })
            .append($("<div>", {
                class: "card card-demo",

            })
                .append($("<img>", {
                    class: "card-img-top",
                    src: paramRes[paramIndex].coverImage,

                }))
                .append($("<div>", {
                    class: "card-body",

                })
                    .append($("<div>", {
                        class: "row",
                    })
                        .append($("<div>", {
                            class: "col-sm-12",
                        })
                            .append($("<h6>", {
                                class: "text-primary font-weight-bold",
                                html: paramRes[paramIndex].courseName,

                            }))
                        )
                    )
                    .append($("<div>", {
                        class: "row pl-3",
                    })
                        .append($("<i>", {
                            class: "far fa-clock mr-2 mt-1",

                        }))
                        .append($("<p>", {
                            html: paramRes[paramIndex].duration,
                        })


                        )
                        .append($("<p>", {
                            class: "ml-3",
                            html: paramRes[paramIndex].level
                        }))
                    )
                    .append($("<div>", {

                    })
                        .append($("<div>", {
                            class: "col-sm-12",
                        })
                            .append($("<p>", {
                                class: "font-weight-bold",
                                html: "$" + paramRes[paramIndex].discountPrice,
                            })
                                .append($("<del>", {
                                    class: "text text-del-money ml-2",
                                    html: "$" + paramRes[paramIndex].price
                                }))
                            )
                        )
                    )
                )
                .append($("<div>", {
                    class: "card-footer",
                })
                    .append($("<div>", {
                        class: "row row-card"
                    })
                        .append($("<div>", {
                            class: "col-sm-3",
                        })
                            .append($("<img>", {
                                src: paramRes[paramIndex].teacherPhoto,
                                class: "rounded-circle",
                                height: "40",
                            }))
                        )
                        .append($("<div>", {
                            class: "col-sm-7 text-left pt-2",
                        })
                            .append($("<p>", {

                                class: "small",
                                html: paramRes[paramIndex].teacherName,

                            }))
                        )
                        .append($("<div>", {
                            class: "col-sm-2  text-right pt-2",
                        })
                            .append($("<i>", {

                                class: "far fa-bookmark",


                            }))
                        )
                    )
                )
            )
        )
    )

}
function getDataPopular(paramRes, paramIndex) {

    $("#row-popular").append($("<div>", {
        class: "col-sm-3",

    })
        .append($("<div>", {
            class: "card card-demo",

        })
            .append($("<div>", {
                class: "card card-demo",

            })
                .append($("<img>", {
                    class: "card-img-top",
                    src: paramRes[paramIndex].coverImage,

                }))
                .append($("<div>", {
                    class: "card-body",

                })
                    .append($("<div>", {
                        class: "row",
                    })
                        .append($("<div>", {
                            class: "col-sm-12",
                        })
                            .append($("<h6>", {
                                class: "text-primary font-weight-bold",
                                html: paramRes[paramIndex].courseName,

                            }))
                        )
                    )
                    .append($("<div>", {
                        class: "row pl-3",
                    })
                        .append($("<i>", {
                            class: "far fa-clock mr-2 mt-1",

                        }))
                        .append($("<p>", {
                            html: paramRes[paramIndex].duration,
                        })


                        )
                        .append($("<p>", {
                            class: "ml-3",
                            html: paramRes[paramIndex].level
                        }))
                    )
                    .append($("<div>", {

                    })
                        .append($("<div>", {
                            class: "col-sm-12",
                        })
                            .append($("<p>", {
                                class: "font-weight-bold",
                                html: "$" + paramRes[paramIndex].discountPrice,
                            })
                                .append($("<del>", {
                                    class: "text text-del-money ml-2",
                                    html: "$" + paramRes[paramIndex].price
                                }))
                            )
                        )
                    )
                )
                .append($("<div>", {
                    class: "card-footer",
                })
                    .append($("<div>", {
                        class: "row row-card"
                    })
                        .append($("<div>", {
                            class: "col-sm-3",
                        })
                            .append($("<img>", {
                                src: paramRes[paramIndex].teacherPhoto,
                                class: "rounded-circle",
                                height: "40",
                            }))
                        )
                        .append($("<div>", {
                            class: "col-sm-7 text-left pt-2",
                        })
                            .append($("<p>", {

                                class: "small",
                                html: paramRes[paramIndex].teacherName,

                            }))
                        )
                        .append($("<div>", {
                            class: "col-sm-2  text-right pt-2",
                        })
                            .append($("<i>", {

                                class: "far fa-bookmark",


                            }))
                        )
                    )
                )
            )
        )
    )

}

